package org.openrsc.editor.model.brush;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.openrsc.editor.model.template.TerrainProperty;
import org.openrsc.editor.model.template.TerrainTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum BrushPresets {

    NONE(
            "None",
            new TerrainTemplate(Collections.emptyMap())
    ),

    CUSTOM(
            "Configure your own",
            null
    ),

    CLEAR_TILE(
            "Delete tile",
            new TerrainTemplate(Arrays.stream(TerrainProperty.values()).collect(Collectors.toMap(prop -> prop, prop -> 0)))
    ),

    ROOF(
            "Roof",
            TerrainTemplate.builder()
                    .value(TerrainProperty.ROOF_TEXTURE, 1)
                    .build()
    ),

    GRASS(
            "Grass",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_TEXTURE, 70)
                    .build()
    ),

    GREY_PATH(
            "Grey path",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 1)
                    .build()
    ),

    WATER(
            "Water",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 2)
                    .build()
    ),

    GRIMY_WATER(
            "Grimy water",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 7)
                    .build()
    ),

    LAVA(
            "Lava",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 11)
                    .build()
    ),

    WOODEN_BRIDGE(
            "Wooden bridge over water",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 4)
                    .build()
    ),

    LAVA_BRIDGE(
            "Wooden bridge over lava",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 12)
                    .build()
    ),

    INVISIBLE_BRIDGE(
            "Invisible bridge over water",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 20)
                    .build()
    ),

    WOODEN_FLOOR(
            "Wooden floor",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 3)
                    .build()
    ),

    DARK_RED_BANK_FLOOR(
            "Dark red bank floor",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 6)
                    .build()
    ),

    CROP_DIRT(
            "Crop dirt",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 23)
                    .build()
    ),

    BLUE(
            "Blue",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 13)
                    .build()
    ),

    Purple(
            "Purple",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 15)
                    .build()
    ),

    BLACK_FLOOR(
            "Black floor",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 16)
                    .build()
    ),

    ICE(
            "Ice",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 17)
                    .build()
    ),

    BLACK_HOLE(
            "Black (not walkable, for stairs)",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 8)
                    .build()
    ),

    Pentagram(
            "Pentagram",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 14)
                    .build()
    ),

    REMOVE_BUILDING(
            "Remove building",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 0)
                    .value(TerrainProperty.ROOF_TEXTURE, 0)
                    .value(TerrainProperty.WALL_NORTH, 0)
                    .value(TerrainProperty.WALL_EAST, 0)
                    .value(TerrainProperty.WALL_DIAGONAL, 0)
                    .build()
    ),

    REMOVE_NORTH_WALL(
            "Remove north wall",
            TerrainTemplate.builder()
                    .value(TerrainProperty.WALL_NORTH, 0)
                    .build()
    ),

    REMOVE_EAST_WALL(
            "Remove east wall",
            TerrainTemplate.builder()
                    .value(TerrainProperty.WALL_EAST, 0)
                    .build()
    ),

    REMOVE_DIAGONAL_WALL(
            "Remove diagonal wall",
            TerrainTemplate.builder()
                    .value(TerrainProperty.WALL_DIAGONAL, 0)
                    .build()
    ),

    REMOVE_OVERLAY(
            "Remove overlay",
            TerrainTemplate.builder()
                    .value(TerrainProperty.GROUND_OVERLAY, 0)
                    .build()
    ),

    REMOVE_ROOF(
            "Remove roof",
            TerrainTemplate.builder()
                    .value(TerrainProperty.ROOF_TEXTURE, 0)
                    .build()
    );

    private final String label;
    private final TerrainTemplate template;

    @Override
    public String toString() {
        return label;
    }
}
