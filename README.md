## 2D RSC Landscape Editor

Released under the General Public License v2. Completely freeware.

> Created by xEnt, Vrunk, & Peter.
> 
> Updated by the Open RuneScape Classic development team.

Note: This project requires JDK 11 or higher to compile within IntelliJ IDEA with Gradle.